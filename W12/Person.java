/**
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 10/12/2018
 */

package composite;

public class Person
{
    private String name;
    private Person partner;
    private ArrayList<Person> children;
    private static ArrayList<Person> people = new ArrayList<Person>();

    public Person(String name, Person... family)
    {
        this.name = name;
        this.children = new ArrayList<Person>();
        for(Person person : family)
            if (partner == null) partner = person;
            else children.add(person);
        people.add(this);
    }

    public static ArrayList<Person> single()
    {
        for(Person person : people)
            if (person.partner == null) person
    }

}