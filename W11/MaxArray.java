/**
 * class MaxArray tim Max trong mang theo lap trinh tong quats
 * @author Nguyen Tuan Linh
 * @since 27/11/2018
 * @version 1.0
 */

import java.util.ArrayList;

public class MaxArray
{
    public static <T extends Comparable> T max(ArrayList<T> arr)
    {
        if(arr==null || arr.size()==0) return null;
        T Max = arr.get(0);
        for (T first : arr)
        {
            if(Max.compareTo(first)<0) Max = first;
        }
        return Max;
    }

    public static void main(String[] args)
    {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(1); arr.add(3); arr.add(2); arr.add(4); arr.add(5);
        System.out.println(max(arr));
        ArrayList<Double> arr1 = new ArrayList<Double>();
        arr1.add(0.1); arr1.add(0.3); arr1.add(0.2); arr1.add(0.4); arr1.add(0.5);
        System.out.println(max(arr1));
    }
}
