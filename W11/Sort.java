/**
 * Class sort de sap xep cac phan tu trong mang theo lap trinh tong quat
 * @author Nguyen Tuan Linh
 * @version 1.0
 * @since 27/11/2018
 */
public class Sort
{
    public static <T extends Comparable> void sort(T[] arr)
    {
        for(int i = 0; i<arr.length; i++)
            for(int j =i+1; j <arr.length;j++)
                if(arr[i].compareTo(arr[j])>0)
                {
                    T tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
    }
    public static <T> void print(T[] arr)
    {
        for(T first : arr)
            System.out.print(first + " ");
        System.out.println();
    }

    public static void main(String[] args)
    {
        Integer[] arr = {1,2,3,1,4,5,2,7,6};
        print(arr);
        sort(arr);
        print(arr);
        Double[] arr2 = {1.2,0.2,3.4,4.5,1.5,4.5,6.0,4.2};
        print(arr2);
        sort(arr2);
        print(arr2);
        String[] arr1 = {"Nguyen", "Tuan", "Linh", "Nguyen", "Nhat", "Linh"};
        print(arr1);
        sort(arr1);
        print(arr1);
    }
}
