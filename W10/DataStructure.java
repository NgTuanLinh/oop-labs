

import Week9.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DataStructure {

    public static int BracketCounter(String s){
        int b = 0;
        int vt = -1;
        while (vt < s.length()){
            vt = s.indexOf('{', vt + 1);
            if (vt != -1){
                b++;
            } else break;
        }
        while (vt < s.length()){
            vt = s.indexOf('}', vt + 1);
                if (vt != -1){
                    b--;
                } else break;
        }
        return b;
    }

    public static List<String> getAllFunctions(String name, File path) throws IOException{
        Utils utils = new Utils();
        String content = utils.readContentFromFile(path.getPath());
        int start = 0;
        int end;
        content += '\n';
        List<String> list = new LinkedList<String>();
        List<Integer> index = new LinkedList<Integer>();
        while (start < content.length())
        {
            end = content.indexOf('\n', start);
            String tmp = content.substring(start, end);    // copy content
            start = end + 1;
            if (tmp.contains(name))
            {
                String functions = tmp + "\n";
                index.add(new Integer(start - tmp.length()));
                int dem = BracketCounter(tmp);
                if (!tmp.contains("{") || dem != 0)     //check if exist function
                {
                    while (start < content.length())
                    {
                        end = content.indexOf('\n', start);
                        tmp = content.substring(start, end);    //substring to get function
                        start = end + 1;
                        functions += tmp + "\n";
                        dem += BracketCounter(tmp);     // "}"
                        if ( dem == 0)  break;
                    }
                }
                list.add(functions);
            }
        }

        int vt = 0;
        start = content.indexOf("/*", 0);   //check function in comment
        while (start <content.length())
        {
            end = content.indexOf("*/",start+2);
            while (index.get(vt).intValue() + 4 <start) vt++;
            // c
            if(index.get(vt).intValue() + 4 >= start && index.get(vt).intValue() + list.get(vt).length() - 4 <= end)
            {
                while (vt < list.size() && index.get(vt).intValue() + list.get(vt).length() - 4 <= end)
                {
                    index.set(vt,new Integer(-1));
                    vt++;
                }
            }
            start = content.indexOf("/*",end + 2);  // check if exist another function
            if (start == -1) break;
        }
        for(int i = list.size() - 1; i >= 0; i--)
            if(index.get(i).intValue() == -1) list.remove(i);
        return list;
    }

    public static String findFunctionByName(String name) throws IOException{
        String subname = name.substring(0, name.indexOf('(', 0) - 1);   //get name until reach "("
        name = name.substring(name.indexOf("(") + 1, name.indexOf(")"));    //variables
        name.trim();
        String[] a = name.split(",");
        List<String> list = getAllFunctions(subname, file);
        for (String s: list){
            String s1 = s.substring(s.indexOf("(") + 1, s.indexOf(")"));
            s1.trim();
            String[] b = s1.split(",");
            if (a.length != b.length){
                continue;
            }
            boolean check = true;
            for (int i = 0; i < a.length; i++){
                if (!b[i].trim().startsWith(a[i].trim())){
                    check = false;
                    break;
                }
            }
            if (!check) continue;
            return s;
        }
        return null;
    }

    public static File file = new File("src/Week9/Utils.java");

    public static void main(String[] args) throws IOException{
        List<String> list = new ArrayList<String>();
        list = getAllFunctions("static", file);
        for (String s :list) {
            System.out.println(s);
        }

        String contentFunction = findFunctionByName("public static void continueWriteContentToFile(String)");
        System.out.println(contentFunction);

    }
}
