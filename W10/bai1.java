/**
 * @author Nguyen Tuan Linh
 * @version 1.0
 * @since 13/11/2018
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class bai1 {
    /**
     * khai bao ten file
     */
    private static final File file = new File("F:/University Learning/Nam 2/OOP/W9/Week_9/ex/Utils.java");
//    public static File findFileByName(String folderPath, String fileName){
//        File folder = new File(folderPath);
//        if (folder.isDirectory()) {
//            File[] listFile = folder.listFiles();
//            for (File eachFile : listFile) {
//                if (eachFile.getName().equals(fileName)) return eachFile;
//            }
//
//        }
//        return null;
//    }

    /**
     * ham getContent de lay noi dung ham
     * @param file ten file truyen vao
     * @return noi dung file
     */
    public static String getContent(File file){
        String res = "";
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                res += sc.nextLine();
                if (sc.hasNextLine()) res+="\n";
            }
        }catch (FileNotFoundException e){
            System.out.println("File not found");
        }
        return res;
    }

    /**
     * ham getAllFunctions de lay noi dung ham static
     * @param path ten file truyen vao
     * @return list ham dc lay noi dung
     */
    public static List<String> getAllFunctions(File path){
        List<String> staticFunctions = new ArrayList<>();
        String content = getContent(path);
        String[] lines = content.split("\n");
        String h;
        int dem = 0;
        boolean check1;
        for (int i = 0; i < lines.length; i++){
            lines[i].trim();
            if(lines[i].contains("//")) {
                int t = lines[i].indexOf("//", 0);
                h = lines[i].substring(0, t);
            }
            else h = lines[i];
            if(lines[i].contains("/*")) {dem+=1;}
            if(lines[i].contains("*/")){dem-=1;}
            if(dem > 0){check1 = false;}
            else check1 = true;
            if(check1 == true) {
                if (h.contains("static") && !lines[i].contains("/*") && !lines[i].contains("//")
                        && !lines[i].contains(";") && !lines[i].contains("*")) {
                    int startPos = i, endPos = 0;
                    Stack<String> check = new Stack<String>();
					check.push ("{");
                    for (int j = i + 1; j < lines.length; j++) {
                        if (lines[j].indexOf("{") != -1) check.push("{");
                        if (lines[j].indexOf("}") != -1) check.pop();
                        if (check.isEmpty()) {
                            endPos = j;
                            break;
                        }
                    }
                    String line = "";
                    for (int k = startPos; k <= endPos; k++) line += lines[k] + "\n";
                    staticFunctions.add(line);
                }
            }
        }
        return staticFunctions;
    }

    /**
     * findfunctionByName tim file theo ten
     * @param path ten file can xet
     * @param name ten ham can tim
     * @return  ham tim duoc
     * @throws IOException
     */
    public static String findFunctionByName(File path, String name)throws IOException
    {

        String subname = name.substring(0,name.indexOf('(',0)-1);
        name = name.substring(name.indexOf("(")+1, name.indexOf(")"));
        name.trim();
        String[] a = name.split(",");
        List<String> list = getAllFunctions(path);
        for(String s :list)
        {
            int start = s.indexOf('(',0);
            int end = s.indexOf(')',0);
            String s2 = s.substring(s.indexOf("(")+1, s.indexOf(")"));
            s2.trim();
            String[] b = s2.split(",");
            if(a.length != b.length) continue;
            boolean check = true;
            for(int j = 0; j <a.length; j++)
            {
                if (!b[j].trim().startsWith(a[j].trim()))
                {
                    check = false;
                    break;
                }
            }
            if(!check) continue;
            return s;
        }
        return null;

    }

    public static void main(String[] args) throws IOException {
        //System.out.println(findFunctionByName(file, "public static File findFileByName(String, String)"));
        System.out.println(getAllFunctions(file));
    }
}
