/*
 * class kiem tra loi ep kieu int sang boolean
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public class ClassCast {
	/*
	 * ham ep kieu int sang boolean
	 * @param so int n
	 * @return gia tri sau khi chuyen
	 * @throws exception
	 */
	public static boolean intToBool(int n) throws Exception{
		if (n < 0 || n > 1) throw new Exception ("ep kieu sai");
		return n == 1;
	}
	
	public static void main(String[] args) throws Exception	{
		intToBool(10);
	}
}
