/*
 * class kiem tra con to null
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public class NullPointer {
	/*
	 * ham in string
	 * @param string
	 * @throws exception
	 */
	public static void printString(String s) throws Exception{
		if (s == null) throw new Exception("khong co xau");
		System.out.println(s);
	}
	
	public static void main(String[] args) throws Exception{
		printString("Phuc");
	}
}
