/*
 * class kiem tra truy cap mang
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
import java.util.*;

public class ArrayIndexOfBound {
	/*
	 * ham in phan tu cua mang
	 * @param mang arr va thu tu i
	 * @thows Exception
	 */
	public static void  printElement(ArrayList arr, int i) throws Exception{
		if (i < 0 || i >= arr.size()) throw new Exception("mang khong co phan tu nay");
		System.out.println(arr.get(i));
	}
	
	public static void main(String[] args) throws Exception {
		ArrayList arr = new ArrayList();
		arr.add(15);
		printElement(arr, 0);
		printElement(arr, 2);
	}
}
