/*
 * class kiem tra ton tai file
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
import java.io.*;
import java.util.Scanner;

public class FileNotFound {
	public static void readFile(String path) throws Exception{
		File file = new File(path);
		if (!file.exists()) throw new Exception("Khong co file");
		Scanner scan  = new Scanner(file);
		int n = scan.nextInt();
		System.out.print(n);
	}
	
	public static void main(String[] args) throws Exception
	{
		readFile("text.txt");
	}
}
