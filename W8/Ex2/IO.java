/*
 * class kiem tra IOException
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */

import java.io.IOException;

public class IO {
	/*
	 * ham check IOException
	 * @throws IOException
	 */
	public static void checkIOE() throws IOException {
		throw new IOException();
	}
	
	public static void main(String[] args) throws IOException {
		checkIOE();
	}
}
