/*
 * class kiem tra chia 0
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public class Arithmetic {
	/*
	 * chia 2 so
	 * @param 2 so int
	 * @throws exception
	 */
	public static void division(int a, int b) throws Exception{
		if (b == 0) throw new Exception("chia cho 0");
		System.out.println(a/b);
	}
	
	public static void main (String[] args) throws Exception {
		division(20, 0);
	}
}
