/*
 * class cac phep toan nhi phan
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public abstract class BinaryExpression extends Expression{
	protected Expression left;
	protected Expression right;
	
	public BinaryExpression(Expression _left, Expression _right) {
		this.left = _left;
		this.right = _right;
	}
	
	public Expression getLeft() {
		return left;
	}
	public void setLeft(Expression _left) {
		this.left = _left;
	}
	public Expression getRight() {
		return right;
	}
	public void setRight(Expression _right) {
		this.right = _right;
	}
	
	/*
	 * in bieu thuc
	 * @param nonce
	 * @return string
	 */
	public abstract String toString();
	
	/*
	 * tinh gia tri bieu thuc
	 * @param nonce
	 * @return int
	 */
	public abstract int evaluate();
}
