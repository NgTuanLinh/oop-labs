/*
 * Tao class Expression
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public abstract class Expression {
	/*
	 * in bieu thuc
	 * @param nonce
	 * @return string
	 */
	public abstract String toString();
	
	/*
	 * tinh gia tri bieu thuc
	 * @param nonce
	 * @return int
	 */
	public abstract int evaluate();
}
