/*
 * class con cua Expression in ra gia tri cua bieu thuc
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public class Numeral extends Expression {
	private int value;
	
	public int getValue() {
		return this.value;
	}
	public void setValue(int _value) {
		this.value = _value;
	}
	
	public Numeral() {
		
	}
	public Numeral(int _value) {
		this.value = _value;
	}
	
	/*
	 * in bieu thuc
	 * @param nonce
	 * @return string
	 */
	public String toString() {
		return String.valueOf(value);
	}
	
	/*
	 * tinh gia tri bieu thuc
	 * @param nonce 
	 * @return int
	 */
	public int evaluate() {
		return value;
	}
}
