/*
 * class test case
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public class ExpressionTest {
	public static void main(String[] args) {
		Expression numeral1 = new Numeral(1);
		Expression square1 = new Square(numeral1);
		Expression numeral2 = new Numeral(5);
		Expression addition1 = new Addition(square1, numeral2);
		Expression square2 = new Square(addition1);
		
		System.out.println(square2.toString() + " = " + square2.evaluate());
		
		try {
			Expression division = new Division(square1, new Numeral(6));
			System.out.print(division.toString() + " = " + division.evaluate());
		}
		catch (Exception e) {
			if (e instanceof ArithmeticException) System.out.println("chia cho 0");
		}
	}
}
