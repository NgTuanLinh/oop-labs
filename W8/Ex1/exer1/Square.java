/*
 * class tinh gia tri binh phuong
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */

import static java.lang.Math.pow;

public class Square extends Expression {
	private Expression express;
	
	public Square(Expression e) {
		express = e;
	}
	
	/*
	 * in bieu thuc
	 * @param nonce
	 * @return string
	 */
	public String toString() {
		return String.format("(%s)^2", express);
	}
	
	/*
	 * tinh gia tri bieu thuc
	 * @param nonce
	 * @return int
	 */
	public int evaluate() {
		return (int) pow(express.evaluate(), 2);
	}
}
