/*
 * class chia 2 bieu thuc
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 23/10/2018
 */
public class Division extends BinaryExpression{
	/*
	 * constructor
	 * @param bieu thuc ve phai va ve trai
	 * @return nonce
	 */
	public Division(Expression _left, Expression _right) {
		super(_left, _right);
	}
	
	/*
	 *	in bieu thuc
	 *@param nonce
	 *@return string
	 */
	public String toString() {
		return String.format("%s / %s", left, right);
	}
	
	/*
	 * tinh gia tri bieu thuc
	 * @param nonce
	 * @return int
	 */
	public int evaluate() {
		return left.evaluate()	/ right.evaluate();
	}
}
