/*
 * khai bao lop con Cam thua ke lop cha hoaQua
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class quaCam extends hoaQua{
    public  quaCam() {}
    public  quaCam(int gb, int sl, String ng, String nn) {
        super(gb, sl, ng, nn);
    }
    /*
     * ---------------
     * Getters & Setters
     */
    public int getSoLuong() {
        return  soLuong;
    }
    public int getGiaBan() {
        return  giaBan;
    }
    public String getNguonGoc() {
        return  nguonGoc;
    }
    public String getNgayNhap() {
        return  ngayNhap;
    }
    /*
     * --------------------
     * Constructor 
     */
    public static void main(String[] args) {
        quaCam qc1 = new quaCam(3000, 10, "Lao", "thu 2");
        System.out.println(qc1.getSoLuong());
        System.out.println(qc1.getGiaBan());
        System.out.println(qc1.getNguonGoc());
        System.out.println(qc1.getNgayNhap());
    }
}
