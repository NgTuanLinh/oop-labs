/*
 * khai bao lop cam Sanh thua ke lop Cam
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class camSanh extends  hoaQua {
    public  camSanh() {}
    public  camSanh(int gb, int sl, String ng, String nn) {
        super(gb, sl, ng, nn);
    }
    /*
     * ---------------
     * Getters & Setters
     */
    public int getSoLuong() {
        return  soLuong;
    }
    public int getGiaBan() {
        return  giaBan;
    }
    public String getNguonGoc() {
        return  nguonGoc;
    }
    public String getNgayNhap() {
        return  ngayNhap;
    }
    /*
     * --------------------
     * Constructor 
     */
    public static void main(String[] args) {
        camSanh cs1 = new camSanh(60000, 20, "Lao", "thu 3");
        System.out.println(cs1.getSoLuong());
        System.out.println(cs1.getGiaBan());
        System.out.println(cs1.getNguonGoc());
        System.out.println(cs1.getNgayNhap());
    }
}
