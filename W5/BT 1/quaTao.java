/*
 * khai bao lop Tao thua ke lop cha Hoa Qua
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class quaTao extends  hoaQua{
    public  quaTao() {}
    public  quaTao(int gb, int sl, String ng, String nn) {
        super(gb, sl, ng, nn);
    }
    /*
     * ---------------
     * Getters & Setters
     */
    public int getSoLuong() {
        return  soLuong;
    }
    public int getGiaBan() {
        return  giaBan;
    }
    public String getNguonGoc() {
        return  nguonGoc;
    }
    public String getNgayNhap() {
        return  ngayNhap;
    }
    /*
     * --------------------
     * Constructor 
     */
    public static void main(String[] args) {
        quaTao qt1 = new quaTao(4000, 7, "Lao", "thu 7");
        System.out.println(qt1.getSoLuong());
        System.out.println(qt1.getGiaBan());
        System.out.println(qt1.getNguonGoc());
        System.out.println(qt1.getNgayNhap());
    }
}
