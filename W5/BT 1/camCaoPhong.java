/*
 * khai bao lop Cam Cao Phong thua ke lop Cam
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class camCaoPhong extends hoaQua{
    public  camCaoPhong() {}
    public  camCaoPhong(int gb, int sl, String ng, String nn) {
        super(gb, sl, ng, nn);
    }
    /*
     * ---------------
     * Getters & Setters
     */
    public int getSoLuong() {
        return  soLuong;
    }
    public int getGiaBan() {
        return  giaBan;
    }
    public String getNguonGoc() {
        return  nguonGoc;
    }
    public String getNgayNhap() {
        return  ngayNhap;
    }
    /*
     * --------------------
     * Constructor 
     */
    public static void main(String[] args) {
        camCaoPhong ccp1 = new camCaoPhong(20000, 30, "Lao", "thu 5");
        System.out.println(ccp1.getSoLuong());
        System.out.println(ccp1.getGiaBan());
        System.out.println(ccp1.getNguonGoc());
        System.out.println(ccp1.getNgayNhap());
    }
}
