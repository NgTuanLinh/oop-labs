/*
 * khai bao lop con Rectangle thua ke lop cha Shape
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class Rectangle extends  Shape{
    private double width = 1.0;
    private double length = 1.0;

    public Rectangle() {}

    public Rectangle(double w, double l) {
        width = w;
        length = l;
    }

    public Rectangle(double w, double l, String cl, boolean f) {
        width = w;
        length = l;
        this.setColor(cl);
        this.setFilled(f);
    }
    /*
     *-----------------------
     *Getters & Setters 
     */
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return width*length;
    }

    public double getPerimeter() {
        return (width + length) *2;
    }

    @Override
    // in de du lieu tai lop Shape
    public String toString() {
        return super.toString();
    }
    /*
     * ----------------------
     * Constructor
     */
    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(500, 300, "red", true);
        System.out.println(rec1.getArea());
        System.out.println(rec1.getPerimeter());
        System.out.println(rec1.getColor());
        System.out.println(rec1.isFilled());

    }
}
