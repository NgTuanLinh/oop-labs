/*
 * khai bao lop cha Shape
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class Shape {
    private String color = new String("red");
    private boolean filled = true;

    public  Shape() {}
    public  Shape(String cl, boolean f) {
        color = cl;
        filled = f;
    }

    /*
     *-----------------------
     *Getters & Setters 
     */
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    // tra lai toan bo du lieu da khai bao trong lop Shape
    public String toString() {
        return color;
    }
}
