/*
 * khai bao lop con Square thua ke lop cha Rectangle( Rectangle thua ke lop Shape)
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class Square extends Rectangle {
    public Square() {}

    public Square(double side) {
        super(side, side);
    }

    public Square(double side, String cl, boolean f) {
        super(side, side);
        this.setColor(cl);
        this.setFilled(f);
    }
    
    /*
     *-----------------------
     *Getters & Setters 
     */
    public double getSide() {
        return super.getWidth();
    }

    public void setSide(double side) {
        super.setWidth(side);
    }

    // in de du lieu tai lop Rectangle
    @Override
    public void setWidth(double width) {
        super.setWidth(width);
		super.setLength(width);
    }
    // in de du lieu tai lop Rectangle 
    @Override
    public void setLength(double length) {
        super.setWidth(length);
		super.setLength(length);
    }
    // in de du lieu tai lop Rectangle
    @Override
    public String toString() {
        return super.toString();
    }
    /*
     * ----------------------
     * Constructor
     */
    public static void main(String[] args) {
        Square sq1 = new Square(9, "green", true);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getColor());
        System.out.println(sq1.isFilled());
        System.out.println(sq1.toString());
    }
}
