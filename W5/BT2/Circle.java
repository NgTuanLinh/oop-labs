/*
 * khai bao lop con Circle thua ke lop cha Shape
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 01-10-2018
 */
public class Circle extends Shape{
    private double radius = 1.0;
    private final double PI = 3.141592;

    public  Circle() {}

    public  Circle(double ra) {
        radius = ra;
    }

    public  Circle(double ra, String cl, boolean f) {
        radius = ra;
        this.setColor(cl);
        this.setFilled(f);
    }

    /*
     *-----------------------
     *Getters & Setters 
     */
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }

    public double getPerimeter() {
        return 2 * PI * radius;
    }
    /*
     * ----------------------
     * Constructor
     */
    public static void main(String[] args) {
        Circle cir1 = new Circle(3, "blue", false);
        System.out.println(cir1.getArea());
        System.out.println(cir1.getPerimeter());
        System.out.println(cir1.getColor());
        System.out.println(cir1.isFilled());
        System.out.println(cir1.toString());
    }
}
