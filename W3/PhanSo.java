/**
 * Class PhanSo
 */
public class PhanSo {
    //Khoi tao Tu so va Mau so
    int tuSo;
    int mauSo;
    //Constructor gan gia tri cho Tu va Mau
    public PhanSo(int tuSo, int mauSo) {
        this.tuSo = tuSo;
        this.mauSo = mauSo;
    }
    // Tim UCLN
    private int greatestDivisor(int a, int b) {
        //Khoi tao 2 so nguyen va dam bao Uoc chung luon lon nhat

        a = Math.abs(a);
        b = Math.abs(b);
        
        while (a != b) {
            if (a > b) {
                a = Math.abs(a - b);
            }
            else {
                // Vi b>a :Swap b voi a
                int c;
                c = a;
                a = b;
                b = c;   
                
            }
        }
        return a;
    }

    /**
     *
     * @param phân số cần tối giản
     * @return phân số đã tối giản
     */
    public PhanSo toiGian(PhanSo phanSo) {
        phanSo.tuSo /= greatestDivisor(phanSo.tuSo, phanSo.mauSo);
        phanSo.mauSo /= greatestDivisor(phanSo.tuSo, phanSo.mauSo);
        return phanSo;
    }

    /**
     * Các hàm cộng trừ nhân chia phân số
     * @param phân số cần tính toán với
     * @return phân số đã được tính toán
     */
    public PhanSo congPhanSo(PhanSo other) {
        int tu = this.tuSo * other.mauSo + other.tuSo * this.mauSo;
        int mau = this.mauSo * other.mauSo;
        PhanSo phanSo = new PhanSo(tu, mau);
        return toiGian(phanSo);
    }

    public PhanSo truPhanSo(PhanSo other) {
        int tu = this.tuSo * other.mauSo - other.tuSo * this.mauSo;
        int mau = this.mauSo * other.mauSo;
        PhanSo phanSo = new PhanSo(tu, mau);
        return toiGian(phanSo);
    }

    public PhanSo nhanPS(PhanSo other) {
        int tu = this.tuSo * other.tuSo;
        int mau = this.mauSo * other.mauSo;
        PhanSo phanSo = new PhanSo(tu, mau);
        return toiGian(phanSo);
    }

    public PhanSo chiaPS(PhanSo other) {
        int tu = this.tuSo * other.mauSo;
        int mau = this.mauSo * other.tuSo;
        PhanSo phanSo = new PhanSo(tu, mau);
        return toiGian(phanSo);
    }
    /**
    Ham so sanh phan so voi 1 phan so khac
    @param 1 phan so khac
    @return 2 phan so co bang nhau khong?
     */
    public boolean equals(PhanSo other) {
        return truPhanSo(other) == new PhanSo(0,0);
    }
    //getter & setter cho mau so
    public int getTuSo() {
        return tuSo;
    }

    public void setTuSo(int tuSo) {
        this.tuSo = tuSo;
    }
    //getter & setter cho mau so
    public int getMauSo() {
        return mauSo;
    }

    public void setMauSo(int mauSo) {
        this.mauSo = mauSo;
    }

    

    
}