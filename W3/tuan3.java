/**
 * @ week3 de tim UCLN va in day Fibonacci
 * @author Nguyen Tuan Linh
 * @version 1.8
 * @2018-09-18
 */

public class tuan3 {
	private static int greatestDivisor(int a, int b) {
		//Tim Uoc chung va chac chan la no lon nhat
		a = Math.abs(a);
		b = Math.abs(b);
		while (a!=b) {
			if (a > b) {
				a = Math.abs(a - b);
				//
			}
			else {
				// Swap b voi a
				int c;
				c = a;
				a = b;
				b = c;
	
			}
		}
		return a;


	}


	private static int getFibonacci(int n) {
		if( n == 0 ) {
			return 0;
		}
		else if (n == 1) {
			return 1;

		}
		else {
			return getFibonacci(n - 1) + getFibonacci(n - 2);
		}
	}

	private static void printFibonacci( int n) {
		for (int i = 0; i < n;i++)
		{
			System.out.print(getFibonacci(i) + " ");

		}
	}	
	
	public static void main(String[] args) {
		System.out.println(greatestDivisor(4, -10));
		printFibonacci(10);

	}

}