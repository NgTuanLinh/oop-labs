/**
 * @ Ham de bieu dien cac do vat
 * @author Nguyen Tuan Linh
 * @version 1.8
 * @2018-09-18
 */


class table{
    // khai boa lop table
    private int dai, rong;
    private String chatlieu;

    //setter
    public void setDai(int dai)
    {
        this.dai = dai;
    }
    //getter
    public int getDai(){
        return this.dai;
    }
    //setter
    public void setRong(int rong){
        this.rong = rong;
    }
    //getter
    public int getRong(){
        return this.rong;
    }
    //setter
    public void setChatlieu(String s){
        this.chatlieu = s;
    }
    //getter
    public String getChatlieu(){
        return this.chatlieu;
    }

}

class paint{
    //khai bao thuoc tinh lop paint
    private String size, color, material;
    //setter
    public  void setSize(String s){
        this.size  = s;
    }
    //getter
    public String getSize(){
        return this.size;
    }
    //setter
    public  void setColor(String s){
        this.color = s;
    }
    //getter
    public String getColor(){
        return this.color;
    }
    //setter
    public  void setMaterial(String s){this.material = s; }
    //getter
    public String getMaterial(){
        return this.material;
    }
}

class Song {
    //khai bao thuoc tinh lop song
    private String ca_sy, nam_sx, tenbai;
    //setter
    public void setTenbai(String s) {
        this.tenbai = s;
    }
    //getter
    public String getTenbai() {
        return this.tenbai;
    }
    //setter
    public void setCa_sy(String s) {
        this.ca_sy = s;
    }
    //getter
    public String getCa_sy() {
        return this.nam_sx;
    }
    //setter
    public void setNam_sx(String s) {
        this.nam_sx = s;
    }
    //getter
    public String getNam_sx() {
        return this.nam_sx;
    }
}

class poet{
    //khai bao thuco tinh lop poet
    private String tacgia;
    //setter
    public void setTacgia(String s) {
        this.tacgia = s;
    }
    //getter
    public String getTacgia() {
        return this.tacgia;
    }
}

class xemay{
    // khai bao thuoc tinh xemay
    private String type;
    //setter
    public void setType(String s) {
        this.type = s;
    }
    //getter

    public String getType() {
        return this.type;
    }

}
class dog{
    //khai bao thuoc tinh lop dog
    private int c_cao, c_nang;
    private String giong_cho;
    //setter
    public void setC_cao(int cao)
    {
        this.c_cao = cao ;
    }
    //getter
    public int getC_cao(){
        return this.c_cao;
    }
    //setter
    public void setC_nang(int nang){
        this.c_nang = nang;
    }
    //getter
    public int getC_nang(){
        return this.c_nang;
    }
    //setter
    public void setGiong_cho(String s){
        this.giong_cho = s;
    }
    //getter
    public String getGiong_cho(){
        return this.giong_cho;
    }

}

class oto{
    // khai bao thuoc tinh oto
    private String hangxe, sochongoi;
    //setter
    public void setHangxe(String s) {
        this.hangxe = s;
    }
    //getter
    public String getHangxe() {
        return this.hangxe;
    }
    //setter
    public void setSochongoi(String s) {
        this.sochongoi = s;
    }
    //getter
    public String getSochongoi() {
        return this.sochongoi;
    }
}

class maybay{
    // khai bao thuoc tinh maybay
    private String hangmaybay;
    //setter
    public void setHangmaybay(String s) {
        this.hangmaybay = s;
    }
    //getter
    public String getHangmaybay() {
        return this.hangmaybay;
    }
}

class board{
    // khai boa thuoc tinh lop board
    private String color, size;
    //setter
    public void setColor(String s) {
        this.color = s;
    }
    //getter
    public String getColor() {
        return this.color;
    }
    //setter
    public void setSize(String s) {
        this.size = s;
    }
    //getter
    public String getSize() {
        return this.size;
    }
}

class market{
    // khai bao thuoc tinh market
    private String diadiem;
    //setter
    public void setDiadiem(String s) {
        this.diadiem = s;
    }
    //getter
    public String getDiadiem() {
        return this.diadiem;
    }
}

public class tuan3_bai3 {
    /**
     *
     * @param args khong su dung
     */
    public static void main(String[] args) {

    }
}
