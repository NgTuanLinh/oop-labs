import static org.junit.jupiter.api.Assertions.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import org.junit.jupiter.api.Test;

class BiggerNumberTest {

	@Test
	void test() {
		BiggerNumber test = new BiggerNumber();
		int res1 = test.biggerNum(5, 6);
		int res2 = test.biggerNum(6, 7);
		int res3 = test.biggerNum(80, 0);
		int res4 = test.biggerNum(-2, 7);
		int res5 = test.biggerNum(-5, -6);
		assertEquals(6,res1);
		assertEquals(7,res2);
		assertEquals(80,res3);
		assertEquals(7,res4);
		assertEquals(-5,res5);
	}

}
