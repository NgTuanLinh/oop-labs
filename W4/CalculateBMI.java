/**
 * @week4 tinh chi so BMI
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 2018-09-25
 */
public class CalculateBMI {
	public String calBMI(double w, double h) {
		double bmi = w/(h*h);
		if (bmi<18.5) return "Thieu can";
		else if (bmi < 24.99) {
			if (bmi > 23) return "Thua can";
			else return "Binh thuong";
		}
		else return "Beo phi";
	}
}	
