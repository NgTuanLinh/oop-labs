/**
 * @week4 tim so lon nhat
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 2018-09-25
 */
public class BiggerNumber {
	public int biggerNum(int a, int b) {
		if (a>=b) return a;
		else return b;
	}
}
