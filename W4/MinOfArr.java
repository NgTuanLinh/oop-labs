/**
 * @week4 tim gia tri min cua mang so nguyen
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 2018-09-25
 */
public class MinOfArr {
	public int minOfArr(int a[], int n) {
		int minNum=a[0];
		for (int i=0;i<n;i++) {
			if (a[i] < minNum)
				minNum = a[i];
		}
		return minNum;
	}
}
