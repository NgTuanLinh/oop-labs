import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateBMITest {

	@Test
	void test() {
		CalculateBMI test = new CalculateBMI();
		String res = test.calBMI(55, 1.7);
		assertEquals(res, "Binh thuong");
		
		res = test.calBMI(70, 1.7);
		assertEquals(res, "Thua can");
		
		res = test.calBMI(90, 1.7);
		assertEquals(res, "Beo phi");
		
		res = test.calBMI(40, 1.7);
		assertEquals(res, "Thieu can");
		
		res = test.calBMI(55, 1.7);
		assertEquals(res, "Binh thuong");
	}

}
