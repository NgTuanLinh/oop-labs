import static org.junit.jupiter.api.Assertions.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.jupiter.api.Test;

class MinOfArrTest {

	@Test
	void test() {
		MinOfArr test = new MinOfArr();
		int a[]= {1,2,3,4,5};
		int n=5;
		int res = test.minOfArr(a, n);
		assertEquals(res,1);
		
		int b[]= {-5,2,3,10,-10};
		res = test.minOfArr(b, n);
		assertEquals(res, -10);
		
		int c[]= {100,2,3,-99,-10};
		res = test.minOfArr(c, n);
		assertEquals(res, -99);
		
		int d[]= {100,200,300,10,-10};
		res = test.minOfArr(d, n);
		assertEquals(res, -10);
		
		int e[]= {0,0,3,10,-5};
		res = test.minOfArr(e, n);
		assertEquals(res, -5);
		
		
	}

}
