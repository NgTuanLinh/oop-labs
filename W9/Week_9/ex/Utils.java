/*
 * Tao class tien ich Utils
 * @author NguyenTuanLinh
 * @version 1.0
 * @since 03-11-2018
*/

import java.io.*;
import java.util.Scanner;

public class Utils {
	/*
	 * doc tu tep txt
	 * @param string to path
	 * @return doc dung file
	 * @throws IOException
	 */
	public static String readContentFromFile(String path) throws IOException {
		String res = "";
		File inFile = new File(path);
		Scanner scan = new Scanner(inFile);
		
		while (scan.hasNextLine())
			res += (scan.nextLine() + "\n");
		
		scan.close();
		
		return res;
	}
	
	/*
	 * xuat noi dung vao file va viet lai file
	 * @param string path to file
	 * @return nonce
	 * @throws IOException
	 */
	public static void replaceContentToFile(String path) throws IOException {
		FileWriter filewrite = new FileWriter(path);
		
		filewrite.write("a b c");
		
		filewrite.close();
	}
	
	/*
	 * bo sung noi dung vao cuoi file
	 * @param string path to file
	 * @return nonce
	 * @throws IOException
	 */
	public static void writeContentToFile(String path) throws IOException {
		FileWriter filewrite = new FileWriter(path, true);
		
		filewrite.write("\nx y z");
		
		filewrite.close();
	}
	
	/*
	 * tim file trong thu muc 
	 * @param string path of folder, string file name
	 * @return File
	 * @throws IOException
	 */
	public static File findByName(String folderPath, String fileName) throws IOException{
		File folder  = new File(folderPath);
		if (!folder.isDirectory())
			return null;
		File[] fileList = folder.listFiles();
		
		for (File res :fileList) {
			if (res.getName().equals(fileName))
				return res;
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		
		try{
			String s = readContentFromFile("test.txt");
		    System.out.println(s);
		    
		    replaceContentToFile("test.txt");
		    
		    writeContentToFile("test.txt");
		    
		    File file = findByName("ex","IO.java");
		    if (file != null) 
		    	System.out.println(file.getAbsolutePath());
		    else 
		    	System.out.println("file doesn't exist");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
