/*
 * khai bao lop con Circle thuoc lop Shape
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 09- 10- 2018
 */

public class Circle extends Shape {
	private double radius;
	
	public Circle() {
		super("circle");
	}
	public Circle(String _color, double _x, double _y) {
		super("circle", _color, _x, _y);
	}
	public Circle(String _color, double _x, double _y, double _radius) {
		super("circle", _color, _x, _y);
		radius = _radius;
	}
	/*
	* ------------------
	* Getters & Setters
	*/
	public void setRadius(double _radius) {
		this.radius = _radius;
	}
	public double getRadius() {
		return this.radius;
	}
	
	public void show() {
		System.out.println(getRadius() + " " + super.getColor() + " " + super.getType() + "(" + super.getX() + "," + super.getY() + ")");
	}
}
