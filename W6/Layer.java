/*
 * khai bao lop Layer
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 09- 10- 2018
 */

import java.util.*;

public class Layer {
	private ArrayList<Shape> shapeArr = new ArrayList<>();
	
	public ArrayList<Shape> getLayer(){
		return this.shapeArr;
	}
	
	/*
	 * xoa triangle trong layer
	 */
	public void delTriangle() {
		for (int i = 0; i < shapeArr.size(); ++i) {
			if(shapeArr.get(i).getType().equals("triangle"))	
				shapeArr.remove(i);
		}
	}
	
	/* 
	 * xoa circle trong layer 
	 */
	public void delCircle() {
		for (int i = 0; i < shapeArr.size(); ++i) {
			if(shapeArr.get(i).getType().equals("circle"))	
				shapeArr.remove(i);
		}
	}
	
	/*
	 * hien thi toan bo shape trong layer
	 */
	public void showAll() {
		for (int i = 0; i < shapeArr.size(); ++i) {
			shapeArr.get(i).show();
		}
	}
	
	/* 
	 * them shape vao layer
	 */
	public void add(Shape _shape	) {
		shapeArr.add(_shape);
	}
}
