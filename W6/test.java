/*
 * test case
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 09- 10- 2018
 */

public class test {
	public static void main(String[] args) {
		Diagram diagram = new Diagram();
		Shape shape = new Rectangle("red", 13, 5, 19, 99);
		Shape triangle = new Triangle("yellow", 3, 4, 3, 4, 5);
		Shape circle = new Circle("blue", 4, 3, 5);
		Layer layer1 = new Layer();
		
		layer1.add(shape);
		layer1.add(triangle);
		layer1.add(circle);
		
		diagram.add(layer1);
		diagram.showAll();
		diagram.delCircle();
		diagram.showAll();
	}
}
