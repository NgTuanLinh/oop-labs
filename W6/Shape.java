/*
 * khai bao lop Shape dai dien cho cac hinh ve khac nhau
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 09- 10- 2018
 */

public class Shape {
	private String type;
	private String color;
	private double x, y;
	
	public Shape(String _type){
		x = y = 0;
		type = _type;
		color = "black";
	}
	
	public Shape(String _type, String _color, double _x, double _y){
		type = _type;
		color = _color;
		x = _x;
		y = _y;
	}
	/*
	* ------------------
	* Getters & Setters
	*/
	public void setType(String _type) {
		this.type = _type;
	}
	public String getType() {
		return this.type;
	}
	
	public void setColor(String _color){
		this.color = _color;
	}
	public String getColor() {
		return this.color;
	}
	
	public void setX(double _x) {
		this.x = _x;
	}
	public double getX() {
		return this.x;
	}
	
	public void setY(double _y) {
		this.y = _y;
	}
	public double getY() {
		return this.y;
	}
	/*
	* hinh ve di chuyen
	*/
	public void move(double deltaX, double deltaY) {
		x += deltaX;
		y += deltaY;
	}
	
	public void show() {
		
	}
}
