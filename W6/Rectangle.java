/*
 * khai bao lop con Rectangle thuoc lop Shape
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 09- 10- 2018
 */
 
public class Rectangle extends Shape {
	private double width, height;
	
	public Rectangle() {
		super("rectangle");
	}
	public Rectangle(String _color, double _x, double _y) {
		super("rectangle", _color, _x, _y);
	}
	public Rectangle(String _color, double _x, double _y, double _width, double _height) {
		super("rectangle", _color, _x, _y);
		width = _width;
		height = _height;
	}
	/*
	* -----------------
	* Getters & Setters
	*/
	public void setWidth(double _width) {
		this.width = _width;
	}
	public double getWidth() {
		return this.width;
	}
	
	public void setHeight(double _height) {
		this.height = _height;
	}
	public double getHeight() {
		return this.height;
	}

	public void show() {
		System.out.println(getHeight() + "x" + getWidth() + super.getColor() + " " + super.getType() + "(" + super.getX() + "," + super.getY() + ")");
	}
}

