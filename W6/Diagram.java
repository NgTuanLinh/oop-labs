/*
 * khai bao lop Diagram dai dien cho so do
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 09- 10- 2018
 */
import java.util.*;

public class Diagram {
	private ArrayList<Layer> layerArr = new ArrayList<>();
	
	public ArrayList<Layer> getDiagram(){
		return this.layerArr;
	}
	
	/* 
	 * xoa circle trong diagram 
	 */
	public void delCircle() {
		for (int i = 0; i < layerArr.size(); ++i) {
			layerArr.get(i).delCircle();
		}
	}
	
	/*
	 * them layer trong diagram
	 */
	public void add(Layer _layer) {
		layerArr.add(_layer);
	}
	
	/*
	 * hien thi toan bo layer trong diagram
	 */
	public void showAll() {
		for (int i = 0; i < layerArr.size(); ++i) {
			layerArr.get(i).showAll();
		}
	}
}
