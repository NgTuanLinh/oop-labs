/*
 * khai bao lop con Triangle thuoc lop Shape
 * @author NguyenTuanLinh
 * @version 1.8
 * @since 09- 10- 2018
 */

public class Triangle extends Shape {
	private double a, b, c;
	
	public Triangle() {
		super("triangle");
	}
	public Triangle(String _color, double _x, double _y) {
		super("triangle", _color, _x, _y);
	}
	public Triangle(String _color, double _x, double _y, double _a, double _b, double _c) {
		super("triangle", _color, _x, _y);
		a = _a;
		b = _b;
		c = _c;
	}
	/*
	* ------------------
	* Getters & Setters
	*/
	public void setA(double _a) {
		this.a = _a;
	}
	public double getA() {
		return this.a;
	}
	
	public void setB(double _b) {
		this.b = _b;
	}
	public double getB() {
		return this.b;
	}
	
	public void setC(double _c) {
		this.c = _c;
	}
	public double getC() {
		return this.c;
	}
	
	public void show() {
		System.out.println(getA() + "x" + getB() + "x" + getC() + " " + super.getColor() + " " + super.getType() + "(" + super.getX() + "," + super.getY() + ")");
	}
}
